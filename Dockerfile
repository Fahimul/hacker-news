# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:12.18.2-alpine3.9 AS build-stage
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
RUN npm run build
# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.19.1-alpine
COPY --from=build-stage /app/build/ /var/www
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
ENTRYPOINT ["nginx","-g","daemon off;"]