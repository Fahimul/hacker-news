import React from 'react';
import {configure , shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Pagination } from './pagination';


configure({adapter: new Adapter()});
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Pagination pageNo={0}/>);
    })


describe('<pagination/>' ,  () => { 
    it('should render only one li for pageNo 0' , () => {

        expect(wrapper.find('ul').children()).toHaveLength(1);
    });

    it('should render three children 2>li and span for pageNo>0' , () => {
        wrapper.setProps({pageNo:2});
        expect(wrapper.find('ul').children()).toHaveLength(3);
    });


    it('should have prev li element for pageNo grate than zero ' , () => {
        wrapper.setProps({pageNo:2});
        expect(wrapper.contains(<li>Prev</li>)).toEqual(true);
    });
});