import React from 'react';
import style from './pagination.module.css';

export const Pagination = (props) => {

    return (
        <div className={style['Pagination-Bar']}>
            <ul>
                {
                    (props.pageNo === 0) ? null :
                        <React.Fragment>
                            <li onClick={props.handlePrevClick}>Prev</li>
                            <span> | </span>
                        </React.Fragment>
                }
                <li onClick={props.handleNextClick}>Next</li>
            </ul>
        </div>
    )
}