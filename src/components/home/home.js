import React from 'react';
import ErrorBoundary from '../common/error-boundry/errorBoundry';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import style from './home.module.css';
import { News } from '../news/news';
import VoteChart from '../chart/voteChart';
import { getData } from '../../utils/ajax'
import { addVoteField, getItems, addVote, hideItem } from '../../utils/storageApi';
import { BASR_URL, STORY } from '../../utils/constants'
import { Pagination } from '../pagination/pagination';

import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "#ff6600",
        color: theme.palette.common.black,
        fontWeight: "bold",
        paddingBottom: 5,
        paddingTop: 10
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        textAlign: "center"
    },
}))(TableRow);

let defaultQueryParam = {
    tags: "story"
};


class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            mappedItem: [],
            pageNo: 0,
            firstLoad: true,
            isLoaded: false,
        };
        this.handlePrevClick = this.handlePrevClick.bind(this);
        this.handleNextClick = this.handleNextClick.bind(this);
        this.addVote = this.addVote.bind(this);
        this.hideItem = this.hideItem.bind(this);
    }

    componentDidMount() {
        const page = this.getParameterByName("p", window.location.href);
        let queryParam = defaultQueryParam;
        if (!(page === null || page === undefined || page === '') && page.match(/^\d+$/)) {
            queryParam = { ...defaultQueryParam, page: page }
        }
        this.fetchData(queryParam);
    }

    componentWillUpdate(nextProps, nextState) {
        if (this.state !== nextState && (this.state.pageNo !== nextState.pageNo) && !this.state.firstLoad) {
            const queryParam = { ...defaultQueryParam, page: nextState.pageNo }
            this.fetchData(queryParam);
        }
    }

    getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
        const results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    fetchData(queryParam) {
        const url = BASR_URL + STORY;
        getData(url, queryParam).then(
            (result) => {
                addVoteField(result);
                this.setState({
                    isLoaded: true,
                    firstLoad: false,
                    items: result,
                    mappedItem: getItems(result),
                    pageNo: result.page
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true
                });
            });
    }

    handlePrevClick(e) {
        this.props.history.push(`/news?p=${(this.state.pageNo) - 1}`)
        this.setState((state, props) => ({
            pageNo: (state.pageNo - 1)
        }));
    }

    handleNextClick(e) {
        this.props.history.push(`/news?p=${(this.state.pageNo) + 1}`)
        this.setState((state, props) => ({
            pageNo: (state.pageNo + 1)
        }));
    }

    addVote(id) {
        console.log(id);
        addVote(id);
        this.setState({ mappedItem: getItems(this.state.items) })
    }

    hideItem(id) {
        hideItem(id);
        this.setState((state, props) => ({ mappedItem: getItems(state.items) }));
        /* this.setState((state, props) => ({
            items: deleteItems(state.items , id)
        })); */
    }


    render() {

        const { items, isLoaded, mappedItem } = this.state;
        const hits = items && items.hits;

        const graphData = mappedItem &&
            mappedItem.filter(e => e.show)
                .map(e => {
                    return { label: e.id, y: e.vote }
                });

        return (
            <ErrorBoundary>
                {(!isLoaded) ? <CircularProgress className={style['SpinnerPosition']} disableShrink /> :
                    <TableContainer className={style.Head} component={Paper}>
                        <Table aria-label="customized table">
                            <TableHead>
                                <TableRow>
                                    <StyledTableCell width="10%" align="center">Comments</StyledTableCell>
                                    <StyledTableCell width="10%" align="center">Vote Count</StyledTableCell>
                                    <StyledTableCell width="10%" align="center">Upvote</StyledTableCell>
                                    <StyledTableCell width="70%" align="left">News Details</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    hits && hits.map((row, index) => (
                                        (mappedItem[index].show) ?
                                            <StyledTableRow key={row.objectID}>
                                                <StyledTableCell align="center">{row.num_comments}</StyledTableCell>
                                                <StyledTableCell align="center">{mappedItem[index].vote}</StyledTableCell>
                                                <StyledTableCell align="center">
                                                    <Tooltip title="Add your vote">
                                                        <IconButton aria-label="Add your vote">
                                                            <ThumbUpIcon onClick={() => { this.addVote(row.objectID) }} className={`Clickable`} />
                                                        </IconButton>
                                                    </Tooltip>
                                                </StyledTableCell>
                                                <StyledTableCell align="left"><News row={row} hideItem={this.hideItem} /></StyledTableCell>
                                            </StyledTableRow> : null
                                    ))
                                }
                            </TableBody>
                        </Table>
                        <Pagination handleNextClick={this.handleNextClick} handlePrevClick={this.handlePrevClick} pageNo={this.state.pageNo} />
                        <VoteChart items={graphData} />
                    </TableContainer>
                }

            </ErrorBoundary>
        )
    }
}

export default Home;