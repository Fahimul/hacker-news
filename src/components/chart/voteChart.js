import React from 'react';
import CanvasJSReact from '../../assets/canvasjs.react';
import style from './voteChart.module.css';
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default class VoteChart extends React.Component{


    render() {
		const options = {
			animationEnabled: true,
			theme: "light2", // "light1", "dark1", "dark2"
			axisY: {
                title: "Votes",
                titleFontWeight:"bold",
			},
			axisX: {
                title: "ID",
                titleFontWeight:"bold",
                labelAngle: -90,
                margin:15,
			},
			data: [{
                type: "line",
				toolTipContent: "ID - {label} , Votes - {y}",
				dataPoints: this.props.items
			}]
		}
		return (
		<div className={style.Chart}>
			<CanvasJSChart options = {options}/>
		</div>
		);
}
}

