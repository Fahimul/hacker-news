import React from 'react';
import style from './news.module.css';


const getHostName = (url) => url.split("//")[1].split("/")[0];

const timeDifference = (current, previous) => {
    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;

    var elapsed = current - previous;

    if (elapsed < msPerMinute) {
        return Math.round(elapsed / 1000) + ' seconds ago';
    }

    else if (elapsed < msPerHour) {
        return Math.round(elapsed / msPerMinute) + ' minutes ago';
    }

    else if (elapsed < msPerDay) {
        return Math.round(elapsed / msPerHour) + ' hours ago';
    }

    else if (elapsed < msPerMonth) {
        return Math.round(elapsed / msPerDay) + ' days ago';
    }

    else if (elapsed < msPerYear) {
        return Math.round(elapsed / msPerMonth) + ' months ago';
    }

    else {
        return Math.round(elapsed / msPerYear) + ' years ago';
    }
}



export const News = (props) => {

    const url = props.row.url && getHostName(props.row.url)
    return (
        <React.Fragment>
            {props.row.title}&nbsp;
            <span className={style.Light}>
                (<a target="_blank" href={props.row.url}>{url}</a>) 
                <br/>
                by <span className={style['Bold-Text']}>{props.row.author}</span> {timeDifference(new Date(), new Date(props.row.created_at))}
                <span onClick={()=>{props.hideItem(props.row.objectID)}} className={`${style['Bold-Text']} Clickable`}>
                &nbsp;[Hide]
                </span>
            </span>
        </React.Fragment>
    )

} 
