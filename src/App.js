import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Home from './components/home/home';

function App() {
  return (
    <Router>
      <Switch>
          <Route path="/news" component={Home} />
          <Redirect to="/news" component={Home}/>
      </Switch>
    </Router>
  );
}

export default App;
