export const addVoteField = (object) => {

    const data = localStorage.getItem("news") && JSON.parse(localStorage.getItem("news"));
    const obj = { ...object, };

    if (data === null) {
        const items = obj.hits.map(element => {
            return { id: element.objectID, show: true, vote: Math.floor(Math.random() * 1000) }
        });
        localStorage.setItem("news", JSON.stringify(items));
    } else {
        const existingIds = data.map(e => e.id);
        obj.hits.forEach(e => {
            if (!(existingIds.includes(e.objectID))) {
                data.push({ id: e.objectID, show: true, vote: Math.floor(Math.random() * 1000) })
            }
        });
        localStorage.setItem("news", JSON.stringify(data));
    }
}

export const getItems = (object) => {
    const data = JSON.parse(localStorage.getItem("news"));
    const ids = object.hits.map(e => e.objectID)
    return data.filter(e => ids.includes(e.id))
}

export const addVote = (id) => {
    const data = JSON.parse(localStorage.getItem("news"));
    let updatedValue = [...data];
    data.forEach((e, index) => {
        if (e.id === id) {
            updatedValue[index].vote += 1;
        }
    });
    localStorage.setItem("news", JSON.stringify(updatedValue));
}

export const hideItem = (id) => {
    const data = JSON.parse(localStorage.getItem("news"));
    let updatedValue = [...data];
    data.forEach((e, index) => {
        if (e.id === id) {
            updatedValue[index].show = false;
        }
    });
    localStorage.setItem("news", JSON.stringify(updatedValue));
}