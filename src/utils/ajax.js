export const getData = async (url, queryparmas) => {
    const response = await fetch(url + '?' + getQueryParam(queryparmas), {
        method: 'GET',
        redirect: 'follow',
    });
    return response.json();
}



const getQueryParam = (queryParmas) => {
    return Object.keys(queryParmas).map(key => key + '=' + queryParmas[key]).join('&');
}
